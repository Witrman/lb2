package barBossHouse;

public class Dish {
    //todo: все поля должна быть приватынми  +
    private double cost;
    private String name;
    //todo: следи за опечатками, идея же сама тебе подчеркивает подозрительные слова)  +
    private String description;
    //todo DEFAULT_COST
    private static final int zero = 0;

    //todo: конструкуторы должны быть публичными +
    public Dish(String name, String description) {
        this(name, description, zero); //todo: не должно быть магических констант в коде, все дефоотные значения+++
        //todo: должны быть объявлены в константах в полях класса и именоваться как константы
    }

    public Dish(String name, String description, double cost) {
        this.name = name;
        this.description = description;
        this.cost = cost;
    }

    //todo: все методы доступа и методы бизнес-логики должны быть публичными.   +
    //todo: Неисплользуемые извне методы должны быть приватными +
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
