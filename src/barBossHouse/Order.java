package barBossHouse;


//todo: нельзя использовать утилитные классы :P
//todo: исключение составляет метод System.arraycopy, но он и не утилитный ))


//todo: на этот класс распрострняются все замечания из Dish
public class Order {
    //todo: поля не должны инициализироваться при объявлении. Это плохо) +
    //todo: К тому же ты size объявил, но нигде его не используешь +
    private int size;
    private Dish[] dishes;

    public Order() {
        //todo: опять магические константы +
        //todo: и да, более узкий констуктор должен вызывать более широкий, переавая ему дефолтное значение, если надо +
        this(16);
    }

    //TODO: нихера не убрал магические константы из кода
    public Order(int size) {
        dishes = new Dish[size];
        this.size = 0;
    }

    //TODO: ну и тут тоже естественно
    public Order(Dish[] dishes) {
        this.dishes = dishes;
        this.size = 0;
    }

    //todo: опять же все методы должны быть публичными
    public boolean add(Dish dish) {
        //todo: из-за того, что ты не используешь size, ты здесь занимаешься херней))
        //todo: приватный сайз позволяет тебе проверить, меньше или равен он длине массива.
        //todo: если меньше, то добавляешь в конец новый элемент, если равен, то увеличиваешь и добавляешь в конец.    +++++++++
        //todo: при этом ты должен гарантировать, что у тебя нет null-Ов между дишами.
        //TODO: size Не может быть больше длины массива ты чо
        //TODO: конвенция устанавливает, что должны быть фигурные скобки даже если один оператор
        if (size >= dishes.length)
            increaseArray();
        dishes[size] = dish;
        size++;
        return true;
    }

    private void increaseArray() {
        //todo: именование так себе))
        Dish[] dishes1 = new Dish[dishes.length * 2];
        System.arraycopy(dishes, 0, dishes1, 0, dishes.length);
        dishes = dishes1;
    }

    //todo: учитывай, что в этом методе ты должен изменять size+

    public boolean remove(String dishName) {
        for (int i = 0; i < size; i++) {
            //todo: нельзя использовать утилитные методы) Переписывай всё без их использования +
            if (dishes[i].getName().equals(dishName)) {
                for (int j = i; j < size; j++) {
                    dishes[j] = dishes[j + 1];
                    //TODO: не совсем верно. То, что теряешь ссылку - правильно, но это не только в конце массива надо делать
                    if (j == size) {
                        dishes[j + 1] = null;
                    }
                }
                size--;
                return true;
            }
        }
        return false;
    }

    public int removeAll(String dishName) {
        int count = 0;
        while (remove(dishName)) {
            count++;
        }
        return count;
    }

    //todo: метод может вернуть актуальный size, не надо заниматься здесь пересчетом++
    public int dishQuantity() {
        return size;
    }

    //todo: без утилитного метода  +
    public int dishQuantity(String dishName) {
        int count = 0;
        //TODO: вот кстати говоря, ходи в цикле со счетчиком до size, так ты не будешь ходить зря по пустым элементам
        for (Dish dish : dishes) {
            if (dish != null && (dish.getName().equals(dishName))) {
                count++;
            }
        }
        return count;
    }

    //todo: скопируй массив с помощью System.arraycopy +
    public Dish[] getDishes() {
        Dish[] bdishes = new Dish[dishQuantity()];
        System.arraycopy(dishes, 0, bdishes, 0, bdishes.length);
        return bdishes;
    }

    public double costTotal() {
        double sum = 0;
        for (Dish dish : dishes) {
            if (dish != null) {
                sum += dish.getCost();
            }
        }
        return sum;
    }

    //    todo: лучше используй дополнительную память чем увеличивать количество операций
//    todo: создай массив размера size, копируй туда уникальные имена, проверяя перед этим, содержится ли ++++++
//    todo: имя в новом массиве или нет (можешь написать приватный метод contains
//    todo: а потом просто обрежешь массив или создашь новый, чтобы не было null-ов
    public String[] dishesNames() {
        int h = 0;
        String[] dishNames = new String[size];
        for (int i = 0; i < size; i++) {
            if (!contains(dishNames, dishes[i].getName())) {
                dishNames[h++] = dishes[i].getName();
            }
        }
        String[] dishNamesResult = new String[h];
        System.arraycopy(dishNames, 0, dishNamesResult, 0, dishNamesResult.length);
        return dishNamesResult;
    }


    private boolean contains(String[] dishNames, String dishName) {
        for (String dn : dishNames) {
            if (dn != null && dn.equals(dishName))
                return true;
        }
        return false;
    }

    //todo: студентам прикладного программирования запрещено писать в этом методе пузырьковую сортировку :P
    //todo: пиши merge))
    public Dish[] sortedDishesByCostDesc() {
        return sortMerge(dishes);
    }

    private static Dish[] sortMerge(Dish[] arr) {
        int len = arr.length;
        if (len < 2) return arr;
        int middle = len / 2;
        return merge(sortMerge(copyLimit(arr, 0, middle)),
                sortMerge(copyLimit(arr, middle, len)));
    }

    private static Dish[] merge(Dish[] array1, Dish[] array2) {
        int length1 = array1.length;
        int length2 = array2.length;
        int a = 0, b = 0;
        int len = length1 + length2;
        Dish[] result = new Dish[len];
        for (int i = 0; i < len; i++) {
            if (b < length2 && a < length1) {
                if (array1[a].getCost() > array2[b].getCost())
                    result[i] = array2[b++];
                else result[i] = array1[a++];
            } else if (b < length2) {
                result[i] = array2[b++];
            } else {
                result[i] = array1[a++];
            }
        }
        return result;
    }

    //TODO: шоблять
    private static Dish[] copyLimit(Dish[] original, int from, int to) {
        Dish[] copy = new Dish[to - from];
        System.arraycopy(original, from, copy, 0,
                Math.min(original.length - from, to - from));
        return copy;
    }


}
